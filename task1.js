function myfunction(number){
    var remainder=0,reversenumber=0;
    while(number>0)
    {
        remainder=number%10;
        reversenumber=(reversenumber*10)+remainder;
        number=number/10;
        number=~~number;//You can also use bitwise operators to truncate the decimal.https://stackoverflow.com/questions/7641818/how-can-i-remove-the-decimal-part-from-javascript-number
    }
    return reversenumber;
}

console.log(myfunction(123))